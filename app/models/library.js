import DS from 'ember-data';
import { match } from '@ember/object/computed';
import { computed, get } from '@ember/object';

const { Model, attr } = DS;
/**
 * Library
 * @see https://docs.particle.io/reference/device-cloud/api/#libraries
 */
export default Model.extend({
  architectures: attr(),
  author: attr(),
  installs: attr(),
  license: attr(),
  name: attr(),
  official: attr(),
  repository: attr(),
  sentence: attr(),
  url: attr(),
  verified: attr(),
  version: attr(),
  /**
   * Whether the library is pre-1.0.0
   * @type {boolean}
   */
  isExperimental: match('version', /^0\./),
  /**
   * Parse repository meta from URL
   * @type {object}
   */
  repositoryMeta: computed('repository', function() {
    const repository = get(this, 'repository');

    if (!repository) {
      return {};
    }

    const parts = repository.split('/');
    const repo = parts.pop().slice(0, -4);
    const owner = parts.pop();

    return {
      owner,
      repo
    };
  })
});
