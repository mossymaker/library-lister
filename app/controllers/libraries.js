import { equal } from '@ember/object/computed';
import Controller from '@ember/controller';
import { set } from '@ember/object';

export default Controller.extend({
  queryParams: ['filter', 'page'],

  filter: '',
  page: 1,

  isFirstPage: equal('page', 1),
  isLastPage: false,
  isLoading: false,

  actions: {
    search(q) {
      set(this, 'filter', q);
      set(this, 'page', 1);
    },
    previous() {
      this.decrementProperty('page');
    },
    next() {
      this.incrementProperty('page');
    }
  }
});
