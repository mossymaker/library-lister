import { set } from '@ember/object';
import { configurable } from 'torii/configuration';
import config from 'library-lister/config/environment';
import OAuth2Bearer from 'torii/providers/oauth2-bearer';

/**
 * Interface with Particle Cloud API OAuth flow
 * @see https://github.com/Vestorly/torii/blob/master/addon/providers/oauth2-code.js
 */
export default OAuth2Bearer.extend({
  name: 'particleio',
  apiKey: configurable('clientId'),
  baseUrl: `${config.apiHost}/oauth/authorize`,

  init() {
    this._super(...arguments);

    set(this, 'responseParams', ['token']);
  }
});
