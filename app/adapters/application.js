import { get } from '@ember/object';
import { isPresent } from '@ember/utils';
import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from 'library-lister/config/environment';

const { JSONAPIAdapter } = DS;

export default JSONAPIAdapter.extend(DataAdapterMixin, {
  host: config.apiHost,
  namespace: config.apiNamespace,

  authorize(xhr) {
    const accessToken = get(this.session, 'data.authenticated.authorizationToken.token');

    if (isPresent(accessToken)) {
      xhr.setRequestHeader('Authorization', `Bearer ${accessToken}`);
    }
  }
});
