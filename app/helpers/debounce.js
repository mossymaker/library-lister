import { helper } from '@ember/component/helper';
import { debounce as _debounce } from '@ember/runloop';

/**
 * Debounce helper
 *
 * It expects a function to invoke, number of milliseconds to wait, and optionally an immediate flag.
 *
 * See Ember docs for more info:
 * https://emberjs.com/api/classes/Ember.run.html#method_debounce
 *
 * @example
 * ```hbs
 * {{some-component onInput=(debounce (action (mut someValue)) 500 true)}}
 * ```
 */
export function debounce([fn, delay, immediate]) {
  return (...args) => _debounce(this, fn, ...args, delay, immediate);
}

export default helper(debounce);
