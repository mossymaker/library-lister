import { set, setProperties } from '@ember/object';
import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

const pageSize = 25;

export default Route.extend(AuthenticatedRouteMixin, {
  queryParams: {
    filter: { refreshModel: true },
    page: { refreshModel: true }
  },

  model({ filter, page }) {
    return this.store.query('library', {
      filter,
      limit: pageSize,
      page,
      scope: 'public',
      sort: 'popularity'
    });
  },
  resetController(controller, isExiting, transition) {
    if (isExiting && transition && transition.targetName !== 'error') {
      setProperties(controller, {
        filter: '',
        isLastPage: false,
        page: 1
      });
    }
  },
  setupController(controller, model) {
    this._super(...arguments);

    const isLastPage = !model.length || model.length < pageSize
    set(controller, 'isLastPage', isLastPage);
  },

  actions: {
    loading(transition) {
      const controller = this.controllerFor('libraries');

      set(controller, 'isLoading', true);
      transition.promise.finally(() => {
        set(controller, 'isLoading', false);
      });
      return true;
    }
  }
});
