import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import fetch from 'fetch';
import { get } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default Route.extend(AuthenticatedRouteMixin, {
  async model() {
    const library = await this._super(...arguments);
    const { owner, repo } = get(library, 'repositoryMeta');
    const options = {
      headers: { 'Accept': 'application/vnd.github.v3.html' }
    };
    const model = { library };

    // Pull in the repo's readme
    if (owner && repo) {
      const readme = (
        await (
          await fetch(`https://api.github.com/repos/${owner}/${repo}/readme`, options)
        ).text()
      );
      model.readme = htmlSafe(readme);
    }

    return model;
  }
});
