import Component from '@ember/component';

export default Component.extend({
  classNames: ['ui', 'inverted', 'blue', 'secondary', 'menu'],
  headerLinkPath: '',
  isLoggedIn: false,
  onLogOut() {}
});
