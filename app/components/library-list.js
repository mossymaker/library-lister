import Component from '@ember/component';

export default Component.extend({
  classNames: ['ui', 'relaxed', 'divided', 'list'],
  isFirstPage: true,
  isLastPage: false,
  libraries: null,
  onNextClick() {},
  onPreviousClick() {}
});
