# Library Lister

An Ember application that lists items in Particle's repository of firmware libraries.

## Installation

* `git clone <repository-url>` this repository
* `cd library-lister`
* `npm install`

## Running / Development

### Hostname

The Particle Cloud API forbids using _localhost_ as a redirect URI; the project uses _particle.local_ which can be mapped to localhost in `/etc/hosts`.

### TLS

The Particle Cloud API requires a redirect URI with the _https_ protocol, so a self-signed TLS certificate is required; generate it on macOS with:

1. The included shell script:

    ```shell
    cd ssl
    ./generate-cert.sh
    ```

1. Add the certificate to Keychain Access and configure its trust settings for SSL.

### Client ID

The Particle Cloud API requires a Client ID:

1. Create a client for development in Particle [Console](https://console.particle.io/authentication) with parameters:
    * Client type: custom
    * Type: web
    * Redirect URI: https://particle.local:4201/torii/redirect.html
    * Scopes: \*:\*
1. Add the generated client ID to the project's _config/environment.js_ under the _torii_ key, e.g.:
    ```
    torii: {
      providers: {
        particleio: {
          clientId: 'library-lister-dev-3230',
          redirectUri: 'https://particle.local:4201/torii/redirect.html'
        }
      }
    }
    ```

### Start

* `npm start`
* Visit your app at https://particle.local:4201.
* Visit your tests at https://particle.local:4201/tests.

## Testing

```
npm test
```

### Notes

* Since authentication with the Particle Cloud API is normally required, the authentication
  request is mocked with _authenticateSession()_
* Mirage is used to mock data
* TLS isn't required and is disabled in the _test_ script

## Deploying

The _master_ branch is deployed to https://mossymaker.gitlab.io/library-lister/.

### CI

A pipeline is set up on GitLab.com at [mossymaker/library-lister/pipelines](https://gitlab.com/mossymaker/library-lister/pipelines).

The project's `.gitlab-ci.yml` specifies _test_, _build_, and _deploy_ stages and saves /dist as a build artifact.

### Variables

* `PARTICLE_CLIENT_ID`: Client ID from Particle [Console](https://console.particle.io/authentication)

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
