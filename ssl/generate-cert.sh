#!/bin/sh

# Generate a cert for particle.local with required SAN fields
openssl req \
  -new \
  -newkey rsa:2048 \
  -x509 \
  -nodes \
  -keyout server.key \
  -out server.crt \
  -subj "/C=US/ST=Oregon/L=Portland/O=/CN=particle.local" \
  -reqexts SAN \
  -extensions SAN \
  -config <(cat /System/Library/OpenSSL/openssl.cnf \
      <(printf '[SAN]\nsubjectAltName=DNS:particle.local,IP:127.0.0.1')) \
  -days 365
