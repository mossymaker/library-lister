import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    const adj = faker.hacker.adjective().capitalize();
    const space = faker.list.random('', ' ')();
    const noun = faker.hacker.noun().capitalize();

    return `${adj}${space}${noun}`;
  },
  id() {
    return faker.helpers.slugify(this.name);
  },
  sentence() {
    return faker.lorem.sentence();
  },
  author() {
    return faker.name.findName();
  },
  installs() {
    return faker.random.number();
  },
  official() {
    return faker.random.boolean();
  },
  verified() {
    return faker.random.boolean();
  },
  version() {
    return `${faker.list.random('0', '1')()}.${faker.random.number(10)}.${faker.random.number(100)}`;
  }
});
