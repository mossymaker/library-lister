import { module, test } from 'qunit';
import { fillIn, visit } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { authenticateSession } from 'ember-simple-auth/test-support';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';

module('Acceptance | search', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('visiting /libraries', async function(assert) {
    server.create('library', {
      name: 'neopixel'
    });

    await authenticateSession({ token: '1234'});
    await visit('/libraries');
    await fillIn('input[data-test=search-input]', 'neo');

    const item = this.element.querySelector('.ui.list .item a');
    assert.ok(item);
    assert.equal(item.textContent, 'neopixel');
  });

  test('visiting /libraries/:id', async function(assert) {
    server.create('library', {
      name: 'neopixel'
    });

    await authenticateSession({ token: '1234'});
    await visit('/libraries/neopixel');

    const header = this.element.querySelector('[data-test=library-header]');
    assert.ok(header);
    assert.ok(header.textContent.startsWith('neopixel'));
  });
});
