import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | library-list', function(hooks) {
  const libraries = [{
    id: 'widget-watcher',
    sentence: 'Watches widgets',
    version: '0.0.1'
  }];

  setupRenderingTest(hooks);

  test('it renders a list of libraries', async function(assert) {
    this.set('libraries', libraries);
    await render(hbs`{{library-list libraries=libraries}}`);

    const items = this.element.querySelectorAll('.item');
    assert.ok(items.length, 1);
  });

  test('it calls an action when the pagination buttons are clicked', async function(assert) {
    assert.expect(2);

    this.set('changePage', () => {
      assert.ok(true);
    });
    this.set('libraries', libraries);

    await render(hbs`
      {{library-list
        isFirstPage=false
        libraries=libraries
        onNextClick=(action changePage)
        onPreviousClick=(action changePage)}}
    `);
    await click('button:first-of-type');
    await click('button:last-of-type');
  });
});
