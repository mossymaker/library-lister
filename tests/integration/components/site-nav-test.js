import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | site-nav', function(hooks) {
  setupRenderingTest(hooks);

  test('it shows a log out button when logged in flag is set', async function(assert) {
    await render(hbs`{{site-nav isLoggedIn=isLoggedIn}}`);
    assert.notOk(this.element.querySelector('button'), '');
    this.set('isLoggedIn', true);
    assert.ok(this.element.querySelector('button'), '');
  });

  test('it calls an action when the log out button is clicked', async function(assert) {
    assert.expect(1);

    this.set('logOut', () => {
      assert.ok(true);
    });
    await render(hbs`{{site-nav isLoggedIn=true onLogOut=(action logOut)}}`);
    await click('button');
  });
});
