import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | libraries', function(hooks) {
  setupTest(hooks);

  test('it updates page property in response to pagination actions', function(assert) {
    let controller = this.owner.lookup('controller:libraries');

    assert.equal(controller.get('page'), 1);
    assert.ok(controller.get('isFirstPage'));

    controller.send('next');
    assert.equal(controller.get('page'), 2);
    assert.notOk(controller.get('isFirstPage'));
  });
});
