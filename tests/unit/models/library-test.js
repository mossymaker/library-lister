import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | library', function(hooks) {
  setupTest(hooks);

  test('it computes properties correctly', function(assert) {
    const store = this.owner.lookup('service:store');
    const model = store.createRecord('library', { version: '0.0.1'});

    assert.ok(model.get('isExperimental'));

    model.set('version', '1.0.0');
    assert.notOk(model.get('isExperimental'));
  });
});
