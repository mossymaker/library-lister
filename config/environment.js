'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'library-lister',
    environment,
    rootURL: '/',
    locationType: 'router-scroll',
    historySupportMiddleware: true,

    apiHost: 'https://api.particle.io',
    apiNamespace: 'v1',
    torii: {
      providers: {
        particleio: {
          clientId: 'library-lister-dev-3230',
          redirectUri: 'https://particle.local:4201/torii/redirect.html'
        }
      }
    },

    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV['ember-cli-mirage'] = {
      enabled: false
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
    ENV.apiHost = "http://localhost:4201";
    ENV.apiNamespace = 'api';
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV.torii.providers.particleio.clientId = process.env.PARTICLE_CLIENT_ID;
    ENV.torii.providers.particleio.redirectUri = 'https://mossymaker.gitlab.io/library-lister/torii/redirect.html';
    ENV.rootURL = '/library-lister';
  }

  return ENV;
};
